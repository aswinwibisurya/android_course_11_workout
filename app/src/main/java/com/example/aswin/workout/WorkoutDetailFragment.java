package com.example.aswin.workout;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class WorkoutDetailFragment extends Fragment {

    private TextView tvName;
    private TextView tvDescription;

    private int id;

    public void setId(int id) {
        this.id = id;
    }

    public WorkoutDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(savedInstanceState != null && savedInstanceState.containsKey("workoutId")) {
            id = savedInstanceState.getInt("workoutId");
        }

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_workout_detail, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        View rootView = getView();
        tvName = rootView.findViewById(R.id.tvName);
        tvDescription = rootView.findViewById(R.id.tvDescription);

        Workout workout = Workout.arrWorkouts[id];

        tvName.setText(workout.name);
        tvDescription.setText(workout.description);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("workoutId", id);
    }
}
